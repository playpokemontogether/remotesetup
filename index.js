const TwitchBot = require('twitch-bot')

const Bot = new TwitchBot({
  username: 'playpokemontogether',
  oauth: 'oauth:lavtshoqguwjz2fpm3c2o4qdy8dipe',
  channels: ['playpokemontogether']
})

Bot.on('join', channel => {
  console.log('Joined channel: ' + channel)
})

Bot.on('error', err => {
  console.log(err)
})


let windows = []
const getWindows = async () => {
  const result = await new Promise(resolve => {
    const { exec } = require('child_process')
    exec(`xdotool search --onlyvisible --name melonDS`, (err, stdout) => {
     resolve(stdout.toString().split('=')[0].split('\n').map(num => parseInt(num, 10)).filter(num => num).sort())
    })
  })
  windows = result
  console.log('result', result)
}
getWindows()

let commandQueue = []

function queue (command) {
   commandQueue.push(command)
}

let isRunning

setInterval(() => {
  if (commandQueue.length && !isRunning) {
    let command = commandQueue.shift()
    const { exec } = require('child_process')
    isRunning = true
    exec(command, async () => {
	    await new Promise(done => setTimeout(done, 650))
	    isRunning = false
    })
  }
}, 150)

Bot.on('message', chatter => {
	if (chatter.message.match(/^[aAbBxXyYlLrRuUdDsStTqQpP]\d?$/)) {
	const command = chatter.message[0].toLowerCase()
        const number = parseInt(chatter.message[1], 10) || 1
        const length = chatter.message.length
		const window = windows[0]
		const bash = `xdotool windowactivate --sync ${window} ${length === 1 ? 'key --delay 150' : 'type --delay 650'} ${[...Array(number || 1)].map(_ => `${command}`).join('')}`
queue(bash)
console.log(bash)

	}
})

Bot.join('playpokemontogether')
